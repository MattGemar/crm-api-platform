import React from 'react'

const Select = ({ name, value, error = '', label, onChange, children }) => {
  return (
    <>
      <label htmlFor={name}>{label}</label>
      <select name={name} id={name} className={'mb-3 form-control' + (error && ' is-invalid')} value={value} onChange={onChange}>
        {children}
      </select>
      {error && <p className='invalid-feedback'> {error}</p>}
    </>
  )
}

export default Select
