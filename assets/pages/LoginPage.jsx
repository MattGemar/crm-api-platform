import React, { useState, useContext } from 'react'
import authApi from '../services/authApi'
import AuthContext from '../contexts/AuthContext'
import Field from '../components/forms/Field'
import { toast } from 'react-toastify'

const LoginPage = ({ history }) => {
  const { setIsAuthenticated } = useContext(AuthContext)

  const [credentials, setCredentials] = useState({
    username: '',
    password: ''
  })
  const [error, setError] = useState('')

  // Gestion des champs
  const handleChange = ({ currentTarget }) => {
    const { value, name } = currentTarget
    setCredentials({ ...credentials, [name]: value })
  }

  // Gestion du submit
  const handleSubmit = async (event) => {
    event.preventDefault()
    try {
      await authApi.authenticate(credentials)
      setError('')
      setIsAuthenticated(true)
      toast.success('Vous êtes connecté')
      history.replace('/customers')
    } catch (error) {
      console.log(error.response)
      setError('Aucun compte ne possède cette adresse ou les information sont érronées')
      toast.error('Une erreur est survenue')
    }
  }

  return (
    <>
      <h1> Connexion à l'application</h1>
      <form onSubmit={handleSubmit}>
        <Field label='Adresse email' name='username' type='email' value={credentials.username} placeholder='Adresse email de connexion' onChange={handleChange} error={error} />
        <Field label='Mot de passe' name='password' type='password' value={credentials.password} onChange={handleChange} error={error} />
        <div className='form-group'>
          <button type='submit' className='btn btn-success'>
            Je me connecte
          </button>
        </div>
      </form>
    </>
  )
}

export default LoginPage
