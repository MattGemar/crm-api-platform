import moment from 'moment'
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { toast } from 'react-toastify'
import TableLoader from '../components/loaders/TableLoader'
import Pagination from '../components/Pagination'
import invoicesAPI from '../services/invoicesAPI'

const STATUS_CLASSES = {
  PAID: 'success',
  SENT: 'primary',
  CANCELED: 'danger'
}

const STATUS_LABELS = {
  PAID: 'Payée',
  SENT: 'Envoyée',
  CANCELED: 'Annulée'
}

const InvoicesPage = (props) => {
  const [invoices, setInvoices] = useState([])
  const [currentPage, setCurrentPage] = useState(1)
  const [search, setSearch] = useState('')
  const itemPerPage = 10
  const [loading, setLoading] = useState(true)

  // Récupération des factures auprès de l'API
  const fetchInvoices = async () => {
    try {
      const data = await invoicesAPI.findAll()
      setInvoices(data)
      setLoading(false)
    } catch (error) {
      toast.error('Une erreur est survenue lors du chargement des factures')
    }
  }

  // Charger les factures au chargement du composant
  useEffect(() => {
    fetchInvoices()
  }, [])

  // Gestion de la suppression
  const handleDelete = async id => {
    const originalinvoices = [...invoices]

    setInvoices(invoices.filter(invoices => invoices.id !== id))

    try {
      await invoicesAPI.delete(id)
      toast.success('La facture à été supprimée')
    } catch (error) {
      toast.error('Une erreur est survenue')
      setInvoices(originalinvoices)
    }
  }

  // Gestion du changement de page
  const handlePageChange = (page) => {
    setCurrentPage(page)
  }

  // Gestion de la recherche
  const handleSearch = ({ currentTarget }) => {
    setSearch(currentTarget.value)
    setCurrentPage(1)
  }

  // Gestion du filtre de recherche
  const filteredInvoices = invoices.filter(
    i =>
      i.customer.firstName.toLowerCase().includes(search.toLowerCase()) ||
      i.customer.lastName.toLowerCase().includes(search.toLowerCase()) ||
      i.amount.toString().includes(search.toLowerCase()) ||
      STATUS_LABELS[i.status].toLowerCase().includes(search.toLowerCase())
  )

  const paginatedInvoices = Pagination.getData(filteredInvoices, currentPage, itemPerPage)

  // Gestion du format de date
  const formatDate = (str) => moment(str).format('DD/MM/YYYY')

  return (
    <>
      <div className='mb3 d-flex justify-content-between align-items-center'>
        <h1>Liste des Factures</h1>
        <Link to='/invoices/new' className='btn btn-primary'>Créer une facture</Link>
      </div>
      <div className='form-group'>
        <input type='text' onChange={handleSearch} value={search} className='form-control' placeholder='Rechercher...' />
      </div>

      <table className='table table-hover'>
        <thead>
          <tr>
            <th>Chrono</th>
            <th>Client</th>
            <th className='text-center'>Date d'envoi</th>
            <th className='text-center'>Statut</th>
            <th className='text-center'>Montant</th>
            <th className='text-center'>Actions</th>
            <th />
          </tr>
        </thead>
        {!loading && <tbody>
          {paginatedInvoices.map(invoice =>
            <tr key={invoice.id}>
              <td>{invoice.chrono}</td>
              <td>
                <Link to={'/customers/' + invoice.customer.id}>{invoice.customer.firstName} {invoice.customer.lastName}</Link>
              </td>
              <td className='text-center'>{formatDate(invoice.sentAt)}</td>
              <td className='text-center'>
                <span className={'badge badge-' + STATUS_CLASSES[invoice.status]}>{STATUS_LABELS[invoice.status]}</span>
              </td>
              <td className='text-center'>{invoice.amount.toLocaleString()} €</td>
              <td className='text-center'>
                <Link to={'/invoices/' + invoice.id} className='btn btn-sm btn-primary mr-1'>Editer</Link>
                <button
                  onClick={() => handleDelete(invoice.id)} className='btn-sm btn-danger'
                >Supprimer
                </button>
              </td>
            </tr>
          )}
        </tbody>}
      </table>
      {loading && <TableLoader />}
      {itemPerPage < filteredInvoices.length && <Pagination currentPage={currentPage} itemPerPage={itemPerPage} length={filteredInvoices.length} onPageChange={handlePageChange} />}
    </>
  )
}

export default InvoicesPage
