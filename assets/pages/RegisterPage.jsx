import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { toast } from 'react-toastify'
import Field from '../components/forms/Field'
import usersApi from '../services/usersApi'

const RegisterPager = ({ history }) => {
  const [user, setUser] = useState({
    firstName: '',
    lastName: '',
    email: '',
    password: ''
  })

  const [errors, setErrors] = useState({
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    passwordConfirm: ''
  })

  // Gestion du changement des imputs dans le formulaire
  const handleChange = ({ currentTarget }) => {
    const { name, value } = currentTarget
    setUser({ ...user, [name]: value })
  }

  const handleSubmit = async (event) => {
    event.preventDefault()

    const apiErrors = {}

    if (user.password !== user.passwordConfirm) {
      apiErrors.passwordConfirm = 'Votre confirmation de mot de passe n\'est pas conforme'
      setErrors(apiErrors)
      toast.error('Une erreur est survenue')
      return
    }

    try {
      await usersApi.register(user)
      toast.success('Vous êtes inscrit, vous pouvez vous connecter')
      setErrors({})
      history.replace('/login')
    } catch (error) {
      console.log(error)
      const { violations } = error.response.data
      if (violations) {
        violations.forEach(violation => {
          apiErrors[violation.propertyPath] = violation.message
        })
      }
      toast.error('Une erreur est survenue')
    }
  }

  return (
    <>
      <h1>Inscription</h1>
      <form onSubmit={handleSubmit}>
        <Field name='firstName' label='Prénom' placeholder='Votre Prénom' value={user.firstName} onChange={handleChange} error={errors.firstName} />
        <Field name='lastName' label='Nom' placeholder='Votre Nom' value={user.lastName} onChange={handleChange} error={errors.lastName} />
        <Field name='email' label='Email' type='email' placeholder='Votre Email' value={user.email} onChange={handleChange} error={errors.email} />
        <Field name='password' label='Mot de Passe' type='password' placeholder='Votre Mot de Passe' value={user.password} onChange={handleChange} error={errors.password} />
        <Field name='passwordConfirm' label=' Confirmer le Mot de Passe' type='password' placeholder='Confirmation Mot de Passe' onChange={handleChange} error={errors.passwordConfirm} />
        <div className='form-group'>
          <button type='submit' className='btn btn-success'>
            Inscription
          </button>
          <Link to='/login' className='btn btn-link'>J'ai déjà un compte</Link>
        </div>
      </form>
    </>
  )
}
export default RegisterPager
