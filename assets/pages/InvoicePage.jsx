import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { toast } from 'react-toastify'
import Field from '../components/forms/Field'
import Select from '../components/forms/Select'
import customersAPI from '../services/customersAPI'
import invoicesAPI from '../services/invoicesAPI'

const InvoicePage = ({ history, match }) => {
  const { id = 'new' } = match.params

  const [invoice, setInvoice] = useState({
    amount: '',
    customer: '',
    status: 'SENT'
  })

  const [errors, setErrors] = useState({
    amount: '',
    customer: '',
    status: ''
  })

  const [editing, setEditing] = useState(false)

  const [customers, setCustomers] = useState([])

  const fetchCustomers = async () => {
    try {
      const data = await customersAPI.findAll()
      setCustomers(data)
      if (!invoice.customer) {
        setInvoice({ ...invoice, customer: data[0].id })
      }
    } catch (error) {
      toast.error('Une erreur est survenue lors du chargement des clients')
      history.replace('/invoices')
    }
  }

  // Récupération d'une facture en fonction de l'identifiant
  const fetchInvoice = async (id) => {
    try {
      const { amount, status, customer } = await invoicesAPI.find(id)
      setInvoice({ amount, customer: customer.id, status })
    } catch (error) {
      toast.error('Une erreur est survenue')
      history.replace('/invoices')
    }
  }

  useEffect(() => {
    fetchCustomers()
  }, [])

  useEffect(() => {
    if (id !== 'new') {
      setEditing(true)
      fetchInvoice(id)
    }
  }, [id])

  // Gestion du changement des imputs dans le formulaire
  const handleChange = ({ currentTarget }) => {
    const { name, value } = currentTarget
    setInvoice({ ...invoice, [name]: value })
  }

  // Gestion de la soumission des imputs dans le formulaire
  const handleSubmit = async (event) => {
    event.preventDefault()
    try {
      if (editing) {
        await invoicesAPI.update(id, invoice)
        toast.success('La facture à bien été modifiée')
      } else {
        await invoicesAPI.create(invoice)
        toast.success('La facture à bien été enregistrée')
        history.replace('/invoices')
      }
      setErrors({})
    } catch ({ response }) {
      const { violations } = response.data
      if (violations) {
        const apiErrors = {}
        violations.forEach(({ propertyPath, message }) => {
          apiErrors[propertyPath] = message
        })
        setErrors(apiErrors)
        toast.error('Une erreur est survenue')
      }
    }
  }

  return (
    <>
      {(!editing && <h1>Création d'une facture</h1>) || <h1>Modification d'une Facture</h1>}
      <form onSubmit={handleSubmit}>
        <Field name='amount' type='number' label='Montant' placeholder='Montant de la facture ' value={invoice.amount} onChange={handleChange} error={errors.amount} />
        <Select name='customer' label='Client' value={invoice.customer} error={errors.customer} onChange={handleChange}>
          {customers.map(customer => <option key={customer.id} value={customer.id}>{customer.firstName} {customer.lastName} </option>)}
        </Select>
        <Select name='status' label='Status' value={invoice.status} error={errors.status} onChange={handleChange}>
          <option value='SENT'> Envoyée </option>
          <option value='PAID'> Payée </option>
          <option value='CANCELLED'> Annulée </option>
        </Select>
        <div className='form-group'>
          <button type='submit' className='btn btn-success'>
            Enregistrer
          </button>
          <Link to='/invoices' className='btn btn-link'>Retourner à la liste</Link>
        </div>
      </form>
    </>
  )
}

export default InvoicePage
