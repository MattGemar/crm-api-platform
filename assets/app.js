import React, { useState } from 'react'
import ReactDOM from 'react-dom'
import { HashRouter, Switch, Route, withRouter } from 'react-router-dom'
/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css'

// start the Stimulus application
import './bootstrap'
import Navbar from './components/Navbar'
import HomePage from './pages/HomePage'
import CustomersPageWithPagination from './pages/CustomersPageWithPagination'
import CustomersPage from './pages/CustomersPage'
import InvoicesPage from './pages/InvoicesPage'
import LoginPage from './pages/LoginPage'
import authApi from './services/authApi'
import AuthContext from './contexts/AuthContext'
import PrivateRoute from './components/PrivateRoute'
import CustomerPage from './pages/CustomerPage'
import InvoicePage from './pages/InvoicePage'
import RegisterPage from './pages/RegisterPage'
import { toast, ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

authApi.setup()

const App = () => {
  const [isAuthenticated, setIsAuthenticated] = useState(authApi.isAuthenticated())

  const NavbarWithRouter = withRouter(Navbar)

  return (
    <AuthContext.Provider value={{
      isAuthenticated,
      setIsAuthenticated
    }}
    >
      <HashRouter>
        <NavbarWithRouter />
        <main className='container pt-5'>
          <Switch>
            <Route path='/login' component={LoginPage} />
            <Route path='/register' component={RegisterPage} />
            <PrivateRoute path='/customers/:id' component={CustomerPage} />
            <PrivateRoute path='/customers' component={CustomersPage} />
            <PrivateRoute path='/invoices/:id' component={InvoicePage} />
            <PrivateRoute path='/invoices' isAuthenticated={isAuthenticated} component={InvoicesPage} />
            <Route path='/test' component={CustomersPageWithPagination} />
            <Route path='/' component={HomePage} />
          </Switch>
        </main>
      </HashRouter>
      <ToastContainer position={toast.POSITION.BOTTOM_LEFT} />
    </AuthContext.Provider>
  )
}

const rootElement = document.querySelector('#app')
ReactDOM.render(<App />, rootElement)
