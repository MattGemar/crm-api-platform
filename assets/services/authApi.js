import axios from 'axios'
import jwtDecode from 'jwt-decode'
import { LOGIN_API } from './config'

/**
 * Positionne le token JWT sur Axios
 * @param {string} token
 */
function setAxiosToken (token) {
  axios.defaults.headers.Authorization = 'bearer ' + token
}

/**
 * Permet de savoir si l'utilisateur est authentifié
 */
function isAuthenticated () {
  // 1.Voir si on a un token
  const token = window.localStorage.getItem('authToken')
  // 2.Voir si le token est valide
  if (token) {
    const { exp: expiration } = jwtDecode(token)
    if (expiration * 1000 > new Date()) {
      // On previent Axios qu'on a maintenant un header par défaut sur les requête
      return true
    }
    return false
  }
  return false
}

/**
 * Mise en place du token au chargement de l'application
 */
function setup () {
  // 1.Voir si on a un token
  const token = window.localStorage.getItem('authToken')
  // 2.Voir si le token est valide
  if (token) {
    const { exp: expiration } = jwtDecode(token)
    if (expiration * 1000 > new Date()) {
      // On previent Axios qu'on a maintenant un header par défaut sur les requête
      setAxiosToken(token)
    }
  }
}

/**
 * Déconnexion (suppression du token dans localStorage et Axios)
 */
function logout () {
  window.localStorage.removeItem('authToken')
  delete axios.defaults.headers.authToken
}

/**
 * Requête HTTP d'authentification et stockage du token dans localstorage et axios
 * @param {object} credentials
 */
function authenticate (credentials) {
  return axios
    .post(LOGIN_API, credentials)
    .then(response => response.data.token)
    .then(token => {
      // Je stocke le token dans le localStorage
      window.localStorage.setItem('authToken', token)
      // On previent Axios qu'on a maintenant un header par défaut sur les requête
      setAxiosToken(token)
      return true
    })
}

export default {
  authenticate,
  logout,
  setup,
  isAuthenticated
}
