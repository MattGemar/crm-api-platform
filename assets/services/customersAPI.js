import axios from 'axios'
import Cache from './cache'
import { CUSTOMERS_API } from './config'

async function findAll () {
  const cachedCustomer = await Cache.get('customers')

  if (cachedCustomer) {
    console.log('utilisation du cache')
    return cachedCustomer
  } else {
    return axios.get(CUSTOMERS_API)
      .then(response => {
        const customers = response.data['hydra:member']
        Cache.set('customers', customers)
        return customers
      })
  }
}

function deleteCustomer (id) {
  return axios.delete(CUSTOMERS_API + '/' + id)
    .then(async response => {
      const cachedCustomer = await Cache.get('customers')
      if (cachedCustomer) {
        console.log('suppression du cache')
        Cache.set('customers', cachedCustomer.filter(c => c.id !== id))
        return cachedCustomer
      }
      return response
    })
}

async function find (id) {
  const cachedCustomer = await Cache.get('customers' + id)

  if (cachedCustomer) {
    return cachedCustomer
  }

  return axios
    .get(CUSTOMERS_API + '/' + id)
    .then(response => {
      const customer = response.data
      console.log('Ajout dans le cache')
      Cache.set('customers' + id, customer)
      return customer
    })
}

function update (id, customer) {
  return axios.put(CUSTOMERS_API + '/' + id, customer)
    .then(async response => {
      const cachedCustomers = await Cache.get('customers')
      const cachedCustomer = await Cache.get('customers' + id)

      if (cachedCustomer) {
        Cache.set('customers' + id, response.data)
      }

      if (cachedCustomers) {
        const index = cachedCustomers.findIndex(c => c.id === +id)
        cachedCustomers[index] = response.data
        console.log('remplacement dans le cache')
        Cache.set('customers', cachedCustomers)
      }
      return response
    })
}

function create (customer) {
  return axios.post(CUSTOMERS_API, customer)
    .then(async response => {
      const cachedCustomer = await Cache.get('customers')
      if (cachedCustomer) {
        console.log('Ajout dans le cache')
        Cache.set('customers', [...cachedCustomer, response.data])
        return cachedCustomer
      }
      return response
    })
}

export default {
  findAll,
  delete: deleteCustomer,
  find,
  update,
  create
}
